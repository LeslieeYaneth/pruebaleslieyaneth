//
//  BoardViewController.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON
import Foundation
import Kingfisher

protocol BoardDisplayLogic: class {
    func displayMovies(_ response: Board.GetMovies.ViewModel)
    func displayMoviesError(_ response: Board.GetMovies.Response)
}

class BoardViewController: BaseViewController {
    // MARK: - Properties
    internal var interactor: (BoardBusinessLogic & BoardDataStore)?
    internal var router: (NSObjectProtocol & BoardRoutingLogic & BoardDataPassing)?
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    var movies: [MovieInfo]! = []
    var selectMovie: MovieInfo!

    // MARK: - Setup
    override func setup() {
        let viewController = self
        let interactor = BoardInteractor()
        let presenter = BoardPresenter()
        let router = BoardRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if MoviesBoard.MoviesBoard == nil {
            interactor!.requestMovies()
        } else {
            movies = MoviesBoard.MoviesBoard
            collectionView.reloadData()
        }
    }
}

// MARK: - Display Logic Methods
extension BoardViewController: BoardDisplayLogic {
    func displayMovies(_ response: Board.GetMovies.ViewModel) {
        MoviesBoard.MoviesBoard = response.movies
        movies = response.movies
        collectionView.reloadData()
    }
    
    func displayMoviesError(_ response: Board.GetMovies.Response) {
        let alert = UIAlertController(title: "Atención", message: response.errorMsg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Collection View Delegate
extension BoardViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellBoardMovie", for: indexPath) as! TabBoardMoviesViewCell
        let resURL = PruebaURLS.movieImagenURL
        cell.movieImg.kf.setImage(with: (resURL + movies[indexPath.row].image).convertStringToURL())
        cell.movieTitleLbl.text = movies[indexPath.row].name
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectMovie = movies[indexPath.row]
        router!.routeToDetail()
    }
}

