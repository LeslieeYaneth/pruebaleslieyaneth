//
//  MainFeedPresenter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON

protocol MainFeedPresentationLogic {
    func presentUserData(_ response: MainFeed.GetUserData.Response)
}

class MainFeedPresenter: MainFeedPresentationLogic {

    // MARK: - Properties
    weak var viewController: MainFeedDisplayLogic?

    // MARK: - Presentation Logic
    func presentUserData(_ response: MainFeed.GetUserData.Response) {
        if response.error {
            viewController?.displayUserDataError(response)
        } else {
            let viewModel = MainFeed.GetUserData.ViewModel(user: response.user)
            viewController?.displayUserData(viewModel)
        }
    }
}
