//
//  MainFeedRouter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON

protocol MainFeedRoutingLogic {
}

protocol MainFeedDataPassing {
    var dataStore: MainFeedDataStore? { get }
}

class MainFeedRouter: NSObject, MainFeedRoutingLogic, MainFeedDataPassing {
    // MARK: Properties
    weak var viewController: MainFeedViewController?
    var dataStore: MainFeedDataStore?
    // MARK: Routing Logic
}
