//
//  MainFeedInteractor.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import PromiseKit
import Disk
import SwiftyJSON

protocol MainFeedBusinessLogic {
    func requestInfo()
}

protocol MainFeedDataStore {
}

class MainFeedInteractor: MainFeedBusinessLogic, MainFeedDataStore {

    // MARK: Properties
    internal var presenter: MainFeedPresentationLogic?
    private let backQueue = DispatchQueue.global(qos: .userInitiated)
    private let worker = UserWorker()
    // MARK: - Interactor Logic
    func requestInfo() {
        worker.infoUser().done { user in
            let response = MainFeed.GetUserData.Response(user: user, error: false, errorMsg: "")
            self.presenter?.presentUserData(response)
        }.catch { error in
            let response = MainFeed.GetUserData.Response(user: nil, error: true, errorMsg: "No fue posible consultar tus datos.")
            self.presenter?.presentUserData(response)
        }
    }
}
