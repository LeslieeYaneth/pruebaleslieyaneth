//
//  DetailMovieViewController.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 16/02/21.
//

import UIKit
import Social

protocol DetailMovieDisplayLogic: class {
}

class DetailMovieViewController: BaseViewController {
    // MARK: - Properties
    internal var interactor: (DetailMovieBusinessLogic & DetailMovieDataStore)?
    internal var router: (NSObjectProtocol & DetailMovieRoutingLogic & DetailMovieDataPassing)?
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var lengthLbl: UILabel!
    @IBOutlet weak var synopsisLbl: UILabel!
    var selectMovie: MovieInfo!

    // MARK: - Setup
    override func setup() {
        let viewController = self
        let interactor = DetailMovieInteractor()
        let presenter = DetailMoviePresenter()
        let router = DetailMovieRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showData()
    }

    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func sharePressed(_ sender: Any) {
        let alert = UIAlertController(title: "Compartir", message: "Compartir esta pelicula", preferredStyle: .actionSheet)
        //facebook
        let actionFacebook = UIAlertAction(title: "Facebook", style: .default) { (action) in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
                let post = SLComposeViewController(forServiceType: SLServiceTypeFacebook)!
                let str = "Mira esta pelicula " + self.selectMovie.name
                post.setInitialText(str)
                self.present(post, animated: true, completion: nil)
            } else {
                self.showAlert(service: "Facebook")
            }
        }
        alert.addAction(actionFacebook)
        self.present(alert, animated: true, completion: nil)
    }

    func showData() {
        showShadow(view: containerView)
        let resURL = PruebaURLS.movieImagenURL
        movieImg.kf.setImage(with: (resURL + selectMovie.image).convertStringToURL())
        nameLbl.text = selectMovie.name
        ratingLbl.text = selectMovie.rating
        genreLbl.text = selectMovie.genre
        lengthLbl.text = selectMovie.length
        synopsisLbl.text = selectMovie.synopsis
    }

    private func showShadow(view: UIView) {
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 1
    }

    func showAlert(service: String) {
        let alert = UIAlertController(title: "Atención", message: "No es posible conectarte a \(service)", preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Display Logic Methods
extension DetailMovieViewController: DetailMovieDisplayLogic {
}
