//
//  DetailMovieInteractor.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 16/02/21.
//

import PromiseKit
import Disk
import SwiftyJSON

protocol DetailMovieBusinessLogic {
}

protocol DetailMovieDataStore {
}

class DetailMovieInteractor: DetailMovieBusinessLogic, DetailMovieDataStore {
    // MARK: Properties
    internal var presenter: DetailMoviePresentationLogic?
    // MARK: - Interactor Logic
}
