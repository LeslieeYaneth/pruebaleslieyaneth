//
//  LoginRouter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

@objc protocol LoginRoutingLogic {
    func routeToMain()
}

protocol LoginDataPassing {
    var dataStore: LoginDataStore? { get }
}

class LoginRouter: NSObject, LoginRoutingLogic, LoginDataPassing {
    weak var viewController: LoginViewController?
    var dataStore: LoginDataStore?

    // MARK: Routing
    func routeToMain() {
        guard let viewC = viewController else { return }
        let comidaMain = UIStoryboard.main.instantiateViewController(withIdentifier: "MainBaseVC") as! MainBaseViewController
        let navController = UINavigationController(rootViewController: comidaMain)
        viewC.present(navController, animated: true, completion: nil)
    }
}
