//
//  LoginPresenter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import Disk

protocol LoginPresentationLogic {
    func presentLogin()
    func presentLoginError()
}

class LoginPresenter: LoginPresentationLogic {
    weak var viewController: LoginDisplayLogic?

    // MARK: - Business Logic
    func presentLogin() {
        viewController?.displayLoginUser()
    }

    func presentLoginError() {
        viewController?.displayLoginUserError()
    }
}
