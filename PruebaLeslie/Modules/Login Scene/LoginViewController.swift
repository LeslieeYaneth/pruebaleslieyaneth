//
//  LoginViewController.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import AccountKit

protocol LoginDisplayLogic: class {
    func displayLoginUser()
    func displayLoginUserError()
}

class LoginViewController: BaseViewController {
    // MARK: - Properties
    internal var interactor: (LoginBusinessLogic & LoginDataStore)?
    internal var router: (NSObjectProtocol & LoginRoutingLogic & LoginDataPassing)?
    // MARK: - Outlets
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var userTxt: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTxt: UITextField!

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    // MARK: - Setup
    override func setup() {
        let viewController = self
        let interactor = LoginInteractor()
        let presenter = LoginPresenter()
        let router = LoginRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor

    }
    // MARK: - Actions
    @IBAction func loginPressed(_ sender: Any) {
        if validateFormSignUp() {
            interactor!.requestActiveOrders(user: userTxt.text!, password: passwordTxt.text!)
            router!.routeToMain()
        }
    }

    // MARK: - Private Methods
    private func setupUI() {
        showShadow(view: userView)
        showShadow(view: passwordView)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    private func validateFormSignUp() -> Bool {
        validateFieldViewSign()
        if userTxt.text!.isEmpty  || passwordTxt.text!.isEmpty {
            showAlert(msg: "Por favor rellena todos los campos." )
            return false
        } else {
            if !userTxt.text!.isValidEmail() {
                showAlert(msg: "Por favor ingresa un correo válido." )
               return false
            }
        }
        return true
    }

    func validateFieldViewSign() {
        if userTxt.text!.count < 2 {
            showShadowError(view: userView)
        } else {
            showShadow(view: userView)
        }
        if passwordTxt.text!.count < 2 {
            showShadowError(view: passwordView)
        } else {
            showShadow(view: passwordView)
        }
    }

    func showShadow(view: UIView) {
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 1
    }

    func showShadowError(view: UIView) {
        view.layer.shadowColor = UIColor.red.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 1
    }

    func showAlert(msg: String) {
        let alert = UIAlertController(title: "Atención", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension LoginViewController: LoginDisplayLogic {
    func displayLoginUser() {
        router!.routeToMain()
    }

    func displayLoginUserError() {
        showAlert(msg: "Por el momento no es posible logearte. Intentalo mas tarde.")
    }
}
