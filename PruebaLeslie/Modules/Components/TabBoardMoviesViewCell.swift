//
//  TabBoardMoviesViewCell.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 16/02/21.
//

import UIKit

class TabBoardMoviesViewCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet weak var movieView: UIView!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieTitleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        movieView.layer.shadowColor = UIColor.gray.cgColor
        movieView.layer.shadowOffset = CGSize(width: 0, height: 1)
        movieView.layer.shadowOpacity = 1
        movieView.layer.shadowRadius = 1
    }
}
