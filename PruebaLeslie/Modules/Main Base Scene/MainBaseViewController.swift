//
//  MainBaseViewController.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

class MainBaseViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var collectionTab: UICollectionView!
    internal var router: (NSObjectProtocol & MainBaseRoutingLogic)?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        router!.routeToMain()
    }
    
    // MARK: - Setup
     func setup() {
        let viewController = self
        let router = MainBaseRouter()
        viewController.router = router
        router.viewController = viewController
    }
}

// MARK: - Collection View Delegate
extension MainBaseViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTabC", for: indexPath) as! TabMenuMainViewCell
        switch indexPath.item {
        case 0:
            cell.titleLbl.text = "Perfil"
        default:
            cell.titleLbl.text = "Cartelera"
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
           router!.routeToMain()

        default:
            router!.routeToBoard()
        }
    }
}
