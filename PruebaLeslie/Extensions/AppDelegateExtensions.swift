//
//  AppDelegateExtensions.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

@available(iOS 13.0, *)
extension AppDelegate {

    // MARK: - Properties
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
