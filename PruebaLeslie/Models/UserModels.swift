//
//  UserModels.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import Foundation
import SwiftyJSON

// Usuario
struct CurrentUser {
    let name: String
    var email: String
    var phone_number: String
    var profile_picture: String
    var card_number: String


    init(json: JSON) {
        name = json["name"].stringValue
        email = json["email"].stringValue
        phone_number = json["phone_number"].stringValue
        profile_picture = json["profile_picture"].stringValue
        card_number = json["card_number"].stringValue
    }
}

// Peliculas
struct MovieInfo {
    let name: String
    var rating: String
    var genre: String
    var length: String
    var synopsis: String
    var video: String
    var image: String

    init(json: JSON) {
        name = json["name"].stringValue
        rating = json["rating"].stringValue
        genre = json["genre"].stringValue
        length = json["length"].stringValue
        synopsis = json["synopsis"].stringValue
        var videoJson = ""
        var imageJson = ""
        for element in json["media"].arrayValue {
            let type = element["type"].stringValue
            if type ==  "video" {
                videoJson = element["resource"].stringValue
            }
            let code = element["code"].stringValue
            if code ==  "poster" {
                imageJson = element["resource"].stringValue
            }
        }
        video = videoJson
        image = imageJson
    }
}
struct Token {
    static var Access_token = ""
    static var Token_type = ""
}

struct MoviesBoard {
    static var MoviesBoard: [MovieInfo]!
}

struct UserProfile {
    static var UserProfile: CurrentUser!
}
